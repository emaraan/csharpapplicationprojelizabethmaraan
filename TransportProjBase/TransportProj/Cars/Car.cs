﻿using System;

namespace TransportProj.Cars
{
    public abstract class Car
    {
        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public Passenger Passenger { get; private set; }
        public City City { get; private set; }

        protected int DestXPos;
        protected int DestYPos;

        protected Car(int xPos, int yPos, City city, Passenger passenger)
        {
            XPos = xPos;
            YPos = yPos;
            City = city;
            City.CityMap[xPos][yPos].Car = this;
            Passenger = passenger;
        }

        public virtual void WritePositionToConsole()
        {
            Console.WriteLine("Car moved to x - {0} y - {1}", XPos, YPos);
        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;
        }

        public void MoveTowardsDestination(Passenger passenger)
        {
            if (this.Passenger == null)
            {
                DestXPos = passenger.StartingXPos;
                DestYPos = passenger.StartingYPos;
            }
            else
            {
                DestXPos = passenger.DestinationXPos;
                DestYPos = passenger.DestinationYPos;
                passenger.DownloadVeyo();
            }
            
            if (this.XPos != DestXPos)
            {
                if (this.XPos < DestXPos)
                {
                    this.MoveRight();
                    return;
                }
                this.MoveLeft();
            }
            else
            {
                if (this.YPos < DestYPos)
                {
                    this.MoveUp();
                    return;
                }
                this.MoveDown();
            }
        }

        public abstract void MoveUp();

        public abstract void MoveDown();

        public abstract void MoveRight();

        public abstract void MoveLeft();

        
    }
}
