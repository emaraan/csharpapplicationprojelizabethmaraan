﻿using System;
using TransportProj.Cars.Honda;

namespace TransportProj.Cars.Factories
{
    public class HondaFactory : ICarFactory
    {
        public Car CreateCar(AppEnum.CarType type, int xPos, int yPos, City city, Passenger passenger)
        {
            switch (type)
            {
                case AppEnum.CarType.Racecar:
                    return new S2000(xPos, yPos, city, passenger);
                case AppEnum.CarType.Sedan:
                    return new Accord(xPos, yPos, city, passenger);
                default:
                    throw new InvalidOperationException();
            }
        }
    }
}
