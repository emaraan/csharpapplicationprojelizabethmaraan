﻿namespace TransportProj.Cars.Factories
{
    public interface ICarFactory
    {
        Car CreateCar(AppEnum.CarType type, int xPos, int yPos, City city, Passenger passenger);
    }
}
