﻿using System;

namespace TransportProj.Cars.Honda
{
    public class Accord : Car
    {
        public Accord(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
        }

        public override void MoveUp()
        {
            if (YPos < City.YMax)
            {
                YPos++;
                WritePositionToConsole();
            }
        }

        public override void MoveDown()
        {
            if (YPos > 0)
            {
                YPos--;
                WritePositionToConsole();
            }
        }

        public override void MoveRight()
        {
            if (XPos < City.XMax)
            {
                XPos++;
                WritePositionToConsole();
            }
        }

        public override void MoveLeft()
        {
            if (XPos > 0)
            {
                XPos--;
                WritePositionToConsole();
            }
        }

        public override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Accord moved to x - {0} y - {1}", XPos, YPos));
        }
    }
}
