﻿using System;

namespace TransportProj.Cars.Honda
{
    public class S2000 : Car
    {
        public S2000(int xPos, int yPos, City city, Passenger passenger) : base (xPos, yPos, city, passenger)
        {
        }

        public override void MoveUp()
        {
            if (YPos < City.YMax)
            {
                YPos = (DestYPos - YPos < 2)
                    ? YPos + 1
                    : YPos + 2;

                WritePositionToConsole();
            }
        }

        public override void MoveDown()
        {
            if (YPos > 0)
            {
                YPos = (YPos - DestYPos < 2)
                    ? YPos - 1
                    : YPos - 2;

                WritePositionToConsole();
            }
        }

        public override void MoveRight()
        {
            if (XPos < City.XMax)
            {
                XPos = (DestXPos - XPos < 2) 
                    ? XPos + 1 
                    : XPos + 2;

                WritePositionToConsole();
            }
        }

        public override void MoveLeft()
        {
            if (XPos > 0)
            {
                XPos = (XPos - DestXPos < 2)
                    ? XPos - 1
                    : XPos - 2;

                WritePositionToConsole();
            }
        }

        public override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("S2000 moved to x - {0} y - {1}", XPos, YPos));
        }
    }
}
