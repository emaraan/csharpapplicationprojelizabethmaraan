﻿
using System.Collections.Generic;
using TransportProj.Cars;
using TransportProj.Cars.Factories;

namespace TransportProj
{
    public class City
    {
        public static int YMax { get; private set; }
        public static int XMax { get; private set; }
        public List<List<CityPosition>> CityMap { get; set; }  

        protected ICarFactory CarFactory;

        public City(int xMax, int yMax, ICarFactory carFactory)
        {
            XMax = xMax;
            YMax = yMax;
            CarFactory = carFactory;

            CityMap = new List<List<CityPosition>>();

            for (var x = 0; x < xMax; x++)
            {
                List<CityPosition> cityPositionRow = new List<CityPosition>();
                for (var y = 0; y < yMax; y++)
                {
                    cityPositionRow.Add(new CityPosition(x, y));
                }
                CityMap.Add(cityPositionRow);
            }
        }

        public Car AddCarToCity(int xPos, int yPos, bool isRacecar)
        {
            return isRacecar 
                ? (Car) CarFactory.CreateCar(AppEnum.CarType.Racecar, xPos, yPos, this , null)
                : CarFactory.CreateCar(AppEnum.CarType.Sedan, xPos, yPos, this, null);
        }

        public Passenger AddPassengerToCity(int startXPos, int startYPos, int destXPos, int destYPos)
        {
            Passenger passenger = new Passenger(startXPos, startYPos, destXPos, destYPos, this);

            return passenger;
        }

    }
}
