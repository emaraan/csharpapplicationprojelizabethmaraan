﻿
using TransportProj.Cars;

namespace TransportProj
{
    public class CityPosition
    {
        public Car Car { get; set; }
        public Passenger Passenger { get; set; }
        public int XPoint { get; set; }
        public int YPoint { get; set; }

        public CityPosition(int xPoint, int yPoint)
        {
            XPoint = xPoint;
            YPoint = yPoint;
        }

        public CityPosition(int xPoint, int yPoint, Car car) : this(xPoint, yPoint)
        {
            Car = car;
        }

        public CityPosition(int xPoint, int yPoint, Passenger passenger) : this(xPoint, yPoint)
        {
            Passenger = passenger;
        }
    }
}
