﻿using System;
using TransportProj.Cars;
using TransportProj.Cars.Factories;

namespace TransportProj
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int CityLength = 10;
            int CityWidth = 10;

            City myCity = new City(CityLength, CityWidth, new HondaFactory());
            Car car = null;

            var carSelectSuccessful = false;
            while(!carSelectSuccessful)
            {
                Console.WriteLine("Please enter 1 for Accord (Sedan) or 2 for S2000 (Racecar) and press [enter].");
                switch (Console.ReadLine())
                {
                    case "1":
                        car = myCity.AddCarToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), false);
                        carSelectSuccessful = true;
                        break;
                    case "2":
                        car = myCity.AddCarToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), true);
                        carSelectSuccessful = true;
                        break;
                }
            }
            
            Passenger passenger = myCity.AddPassengerToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), rand.Next(CityLength - 1), rand.Next(CityWidth - 1));

            Console.WriteLine("\n\nPlease press [enter] for each tick");
            Console.WriteLine("Car started at: (" + car.XPos + ", " + car.YPos + ") ");
            Console.WriteLine("Passenger is at: (" + passenger.StartingXPos + ", " + passenger.StartingYPos + ") ");

            while(!passenger.IsAtDestination())
            {
                Console.ReadLine();
                Tick(car, passenger);
            }

            Console.ReadLine();
            passenger.GetOutOfCar();

            return;
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static void Tick(Car car, Passenger passenger)
        {

            if (car.Passenger == null && car.XPos == passenger.StartingXPos && car.YPos == passenger.StartingYPos)
            {
                passenger.GetInCar(car);
                Console.WriteLine("Passenger's destination is: (" + passenger.DestinationXPos + ", " + passenger.DestinationYPos + ")");
                return;
            }

            // TODO: Add passenger.GetOutOfCar() as part of Tick

            car.MoveTowardsDestination(passenger);
        }

        

    }
}
